/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.impl;

import static net.coalevo.bot.bitly.impl.BitlyConstants.*;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.ArrayList;

/**
 * This class implments a container for bit.ly provided info.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BitlyInfo implements StreamReadable {

  private int m_Error = 0;
  private String m_ErrorMessage = "";

  private String m_Hash;
  private String m_UserHash;

  private String m_HTMLTitle;
  private String m_HTMLMetaDescription;
  private List<String> m_HTMLMetaKeywords;

  private String m_ShortenUser;
  private String m_LongURL;
  private String m_Keyword;
  private List<String> m_Keywords;

  BitlyInfo(String hash) {
    m_Hash = hash;
    m_Keywords = new ArrayList<String>();
    m_HTMLMetaKeywords = new ArrayList<String>();
  }//constructor

  public int getError() {
    return m_Error;
  }//getError

  public String getErrorMessage() {
    return m_ErrorMessage;
  }//getErrorMessage

  public boolean notError() {
    return m_Error == 0;
  }//notError

  public String getHash() {
    return m_Hash;
  }//getHash

  public String getShort() {
    return BITLY_URL + m_Hash;
  }//getShort

  public String getHTMLMetaDescription() {
    return m_HTMLMetaDescription;
  }//getHTMLMetaDescription

  public List<String> getHTMLMetaKeywords() {
    return m_HTMLMetaKeywords;
  }//getHTMLMetaKeywords

  public String getHTMLTitle() {
    return m_HTMLTitle;
  }//getHTMLTitle

  public String getKeyword() {
    return m_Keyword;
  }//getKeyword

  public List<String> getKeywords() {
    return m_Keywords;
  }//getKeywords

  public String getLong() {
    return m_LongURL;
  }//getLong

  public String getShortenUser() {
    return m_ShortenUser;
  }//getShortenUser

  public String getUserHash() {
    return m_UserHash;
  }//getUserHash

  public void readFrom(InputStream in) throws IOException {
    JsonFactory f = new JsonFactory();
    JsonParser jp = f.createJsonParser(in);
    if (jp.nextToken() != JsonToken.START_OBJECT) {
      throw new IOException("Malformed stream.");
    }
    while (jp.nextToken() != JsonToken.END_OBJECT) {
      String fieldname = jp.getCurrentName();
      jp.nextToken(); // move to value
//      System.out.println("Field: " + fieldname);
      if (ERROR_CODE.equals(fieldname)) {
        m_Error = jp.getIntValue();
        if (m_Error != 0) {
          jp.nextToken(); //move to next field
          fieldname = jp.getCurrentName();
          if (ERROR_MESSAGE.equals(fieldname)) {
            jp.nextToken(); //move to value
            m_ErrorMessage = jp.getText();
          }
          //done parsing
          return;
        }
      } else if (ERROR_MESSAGE.equals(fieldname)) {
        m_ErrorMessage = jp.getText();
      } else if (RESULTS.equals(fieldname)) { // contains an object
        while (jp.nextToken() != JsonToken.END_OBJECT) {
          fieldname = jp.getCurrentName();
          jp.nextToken(); //move to value
          //System.out.println("SubField: " + fieldname);
          if (fieldname.equals(m_Hash)) {
            while (jp.nextToken() != JsonToken.END_OBJECT) {
              fieldname = jp.getCurrentName();
              jp.nextToken(); //move to value
              if (GLOBALHASH.equals(fieldname)) {
                m_Hash = jp.getText();
              } else if (HTML_META_DESC.equals(fieldname)) {
                m_HTMLMetaDescription = jp.getText();
              } else if (HTML_META_KEYWORDS.equals(fieldname)) {
                if(jp.getCurrentToken() == JsonToken.START_ARRAY) {
                  while (jp.nextToken() != JsonToken.END_ARRAY) {
                    m_HTMLMetaKeywords.add(jp.getText());
                  }
                }
              } else if (HTML_TITLE.equals(fieldname)) {
                m_HTMLTitle = jp.getText();
              } else if (KEYWORD.equals(fieldname)) {
                m_Keyword = jp.getText();
              } else if (KEYWORDS.equals(fieldname) && jp.getCurrentToken() == JsonToken.START_ARRAY) {
                while (jp.nextToken() != JsonToken.END_ARRAY) {
                  m_Keywords.add(jp.getText());
                }
              } else if (LONG_URL.equals(fieldname)) {
                m_LongURL = jp.getText();
              } else if (SHORTENED_BY_USER.equals(fieldname)) {
                m_ShortenUser = jp.getText();
              } else if (USERHASH.equals(fieldname)) {
                m_UserHash = jp.getText();
              }
            }
          } else {
          }
        }
      } else if (STATUS_CODE.equals(fieldname) && !"OK".equals(jp.getText())) {
        throw new IOException("Failed operation.");
      }
    }
  }//readFrom

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("BitlyInfo");
    sb.append("{m_Hash='").append(m_Hash).append('\'');
    sb.append(", m_HTMLMetaDescription='").append(m_HTMLMetaDescription).append('\'');
    sb.append(", m_HTMLMetaKeywords='").append(m_HTMLMetaKeywords).append('\'');
    sb.append(", m_HTMLTitle='").append(m_HTMLTitle).append('\'');
    sb.append(", m_Keyword='").append(m_Keyword).append('\'');
    sb.append(", m_Keywords=").append(m_Keywords);
    sb.append(", m_LongURL='").append(m_LongURL).append('\'');
    sb.append(", m_ShortenUser='").append(m_ShortenUser).append('\'');
    sb.append(", m_UserHash='").append(m_UserHash).append('\'');
    sb.append('}');
    return sb.toString();
  }//toString
  
/*
  public static void main(String[] args) {
   String str = " {\n" +
       "   \"errorCode\": 0,\n" +
       "   \"errorMessage\": \"\",\n" +
       "   \"results\": {\n" +
       "       \"3j4ir4\": {\n" +
       "           \"globalHash\": \"3j4ir4\",\n" +
       "           \"htmlMetaDescription\": \"\",\n" +
       "           \"htmlMetaKeywords\": \"\",\n" +
       "           \"htmlTitle\": \"Google\",\n" +
       "           \"keyword\": \"\",\n" +
       "           \"keywords\": [],\n" +
       "           \"longUrl\": \"http://google.com/\",\n" +
       "           \"shortenedByUser\": \"bitly\",\n" +
       "           \"userHash\": \"\"\n" +
       "       }\n" +
       "   },\n" +
       "   \"statusCode\": \"OK\"\n" +
       "}";

    try {
      BitlyInfo bi = new BitlyInfo("3j4ir4");
      bi.readFrom(new java.io.ByteArrayInputStream(str.getBytes("utf-8")));
      System.out.println(bi.toString());
    } catch (Exception ex) {
      ex.printStackTrace(System.out);
    }
  }

*/

}//class BitlyInfo
