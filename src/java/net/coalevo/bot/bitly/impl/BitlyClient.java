/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.impl;

import net.coalevo.bot.bitly.service.BitlyBotConfiguration;
import static net.coalevo.bot.bitly.impl.BitlyConstants.*;

import java.net.URLEncoder;
import java.net.URL;
import java.io.UnsupportedEncodingException;
import java.io.IOException;

/**
 * This class implements a client for bit.ly.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BitlyClient {

  private String m_Login;
  private String m_APIKey;

  BitlyClient() {

  }//constructor

  BitlyClient(String login, String apikey) {
    m_Login = login;
    m_APIKey = apikey;
  }//constructor

  public void setAuthentication(String login, String apikey) {
    m_Login = login;
    m_APIKey = apikey;
  }//setAuthentication

  public BitlyURL shorten(String lurl) throws IOException {
    //assemble get
    BitlyURL burl = BitlyURL.createShorten(lurl);

    String req = new StringBuilder(REQ_URL)
        .append(METHOD_SHORTEN)
        .append(PARAMS)
        .append(VERSION).append(EQUALS).append(BitlyBotConfiguration.BITLY_API_VERSION)
        .append(AND).append(LOGIN).append(EQUALS).append(m_Login)
        .append(AND).append(API_KEY).append(EQUALS).append(m_APIKey)
        .append(AND).append(LONG_URL).append(EQUALS)
        .append(encodeURL(lurl)).toString();
    HttpClient.get(new URL(req), burl);
    return burl;
  }//shorten

  public BitlyURL expand(String str) throws IOException {
    BitlyURL burl;
    //assemble get
    if(str.startsWith("http://")) {
      String hash = str.substring(str.lastIndexOf('/')+1);
      burl = BitlyURL.createExpand(hash);
    } else {
      burl = BitlyURL.createExpand(str);
    }
    String req = new StringBuilder(REQ_URL)
        .append(METHOD_EXPAND)
        .append(PARAMS)
        .append(VERSION).append(EQUALS).append(BitlyBotConfiguration.BITLY_API_VERSION)
        .append(AND).append(LOGIN).append(EQUALS).append(m_Login)
        .append(AND).append(API_KEY).append(EQUALS).append(m_APIKey)
        .append(AND).append(HASH).append(EQUALS)
        .append(burl.getHash()).toString();
    HttpClient.get(new URL(req),burl);
    return burl;
  }//expand

  public BitlyStats stats(String str) throws IOException {
    String hash;
    if(str.startsWith("http://")) {
      hash = str.substring(str.lastIndexOf('/')+1);
    } else {
      hash = str;
    }
    BitlyStats bs = new BitlyStats(hash);
    String req = new StringBuilder(REQ_URL)
        .append(METHOD_STATS)
        .append(PARAMS)
        .append(VERSION).append(EQUALS).append(BitlyBotConfiguration.BITLY_API_VERSION)
        .append(AND).append(LOGIN).append(EQUALS).append(m_Login)
        .append(AND).append(API_KEY).append(EQUALS).append(m_APIKey)
        .append(AND).append(HASH).append(EQUALS)
        .append(bs.getHash()).toString();
    HttpClient.get(new URL(req), bs);
    return bs;
  }//stats

  public BitlyInfo info(String str) throws IOException {
    String hash;
    if(str.startsWith("http://")) {
      hash = str.substring(str.lastIndexOf('/')+1);
    } else {
      hash = str;
    }
    BitlyInfo bi = new BitlyInfo(hash);
    String req = new StringBuilder(REQ_URL)
        .append(METHOD_INFO)
        .append(PARAMS)
        .append(VERSION).append(EQUALS).append(BitlyBotConfiguration.BITLY_API_VERSION)
        .append(AND).append(LOGIN).append(EQUALS).append(m_Login)
        .append(AND).append(API_KEY).append(EQUALS).append(m_APIKey)
        .append(AND).append(HASH).append(EQUALS).append(bi.getHash())
        .append(AND).append(KEYS).append(EQUALS).append(INFO_KEYS)
        .toString();
    HttpClient.get(new URL(req), bi);
    return bi;
  }//stats

  static final String encodeURL(String url) {
    try {
      return URLEncoder.encode(url, "utf-8");
    } catch (UnsupportedEncodingException e) {
      Activator.log().error("encodeURL()", e);
      return url;
    }
  }//encodeURL


  public static void main(String[] args) {
    try {
      BitlyClient bc = new BitlyClient("bitlyapidemo","R_0da49e0a9118ff35f52f629d2d71bf07");
      System.out.println(bc.info("http://bit.ly/8zqcJP"));

//      System.out.println(bc.shorten("http://cnn.com").toString());
//      System.out.println(bc.expand("15DlK"));
//      System.out.println(bc.expand("http://bit.ly/15DlK"));
//      System.out.println(bc.stats("http://bit.ly/15DlK"));
//      System.out.println(bc.info("http://bit.ly/15DlK"));
    } catch (IOException ex) {
      ex.printStackTrace(System.out);
    }
  }//main


  static final String REQ_URL = BitlyBotConfiguration.BITLY_URL + "/";
  static final char EQUALS = '=';
  static final char AND = '&';
  static final char PARAMS = '?';

}//class BitlyClient
