/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.impl;

import java.io.*;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.util.Random;

/**
 * This class implements utility methods for working with
 * REST services.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class HttpClient {

  private static Random c_Random = new Random();

  /**
   * Utility class should not be constructed.
   */
  private HttpClient() {
    throw new UnsupportedOperationException();
  }//constructor

  /**
   * Posts the content of the given file to the given index endpoint.
   *
   * @param to    the URL of the SOLR update resource.
   * @param f     the file, which content should be posted.
   * @param ctype the file type that should be used for posting.
   * @return true if successful, false otherwise.
   * @throws IOException if an I/O error occurs.
   */
  public static boolean post(URL to, File f, String ctype)
      throws IOException {

    //1. Prepare
    String boundary = generateBoundary();
    String head = prepareMPHead(boundary, f.getName(), ctype);
    String foot = prepareMPFoot(boundary);
    String clength = Long.toString(f.length() + head.length() + foot.length());

    HttpURLConnection conn = (HttpURLConnection) to.openConnection();
    conn.setDoOutput(true);

    //chunked
    conn.setChunkedStreamingMode(16384);

    conn.setRequestMethod(HTTP_POST);
    conn.setRequestProperty(USER_AGENT, USER_AGENT_STRING);
    conn.setRequestProperty(CONTENT_TYPE, "multipart/form-data; boundary=" + boundary);
    conn.setRequestProperty(CONTENT_LENGTH, clength);
    OutputStream out = conn.getOutputStream();
    PrintWriter pw = new PrintWriter(out);

    //1. Header
    pw.print(head);
    pw.flush();

    //2. File
    copyFile(f, out);

    //3. Foot
    pw.print(foot);
    pw.flush();

    out.flush();
    out.close();
    return conn.getResponseCode() == 200;
  }//post

  /**
   * Posts the content of the given file to the given index endpoint.
   *
   * @param to    the URL of the SOLR update resource.
   * @param data  the data, byte array holding the content that should be posted.
   * @param ctype the file type that should be used for posting.
   * @return true if successful, false otherwise.
   * @throws IOException if an I/O error occurs.
   */
  public static boolean post(URL to, byte[] data, String ctype)
      throws IOException {

    //1. Prepare
    String boundary = generateBoundary();
    String head = prepareMPHead(boundary, "sidecar.xml", ctype);
    String foot = prepareMPFoot(boundary);
    String clength = Long.toString(data.length + head.length() + foot.length());

    HttpURLConnection conn = (HttpURLConnection) to.openConnection();
    conn.setDoOutput(true);

    //chunked
    conn.setChunkedStreamingMode(16384);

    conn.setRequestMethod(HTTP_POST);
    conn.setRequestProperty(USER_AGENT, USER_AGENT_STRING);
    conn.setRequestProperty(CONTENT_TYPE, "multipart/form-data; boundary=" + boundary);
    conn.setRequestProperty(CONTENT_LENGTH, clength);

    OutputStream out = conn.getOutputStream();
    PrintWriter pw = new PrintWriter(out);

    //1. Header
    pw.print(head);
    pw.flush();

    //2. File
    out.write(data);

    //3. Foot
    pw.print(foot);
    pw.flush();

    out.flush();
    out.close();
    return conn.getResponseCode() == 200;
  }//post

  public static boolean get(URL from, StreamReadable r)
      throws IOException {

    HttpURLConnection conn = (HttpURLConnection) from.openConnection();
    conn.setRequestMethod(HTTP_GET);
    conn.setRequestProperty(USER_AGENT, USER_AGENT_STRING);
    InputStream in = conn.getInputStream();
    r.readFrom(in);
    in.close();
    return conn.getResponseCode() == 200;
  }//get

  public static boolean invoke(URL f)
      throws IOException {

    HttpURLConnection conn = (HttpURLConnection) f.openConnection();
    conn.setRequestMethod(HTTP_GET);
    conn.setRequestProperty(USER_AGENT, USER_AGENT_STRING);
    return conn.getResponseCode() == 200;
  }//get

  private static void copyFile(File f, OutputStream out) throws IOException {
    FileChannel source = new FileInputStream(f).getChannel();
    WritableByteChannel dest = Channels.newChannel(out);
    source.transferTo(0, f.length(), dest);
    source.close();
    dest.close();
  }//copyFile

  private static String prepareMPHead(String boundary, String fn, String ctype) {
    StringBuilder sb = new StringBuilder();
    sb.append("--");
    sb.append(boundary);
    sb.append(CRLF);
    sb.append("Content-Disposition: form-data; name=\"filename\"; filename=\"");
    sb.append(fn);
    sb.append("\"");
    sb.append(CRLF);
    sb.append("Content-Type: ");
    sb.append(ctype);
    sb.append(CRLF);
    return sb.toString();
  }//prepareMPHead

  private static String prepareMPFoot(String boundary) {
    StringBuilder sb = new StringBuilder();
    sb.append(CRLF);
    sb.append("--");
    sb.append(boundary);
    sb.append("--");
    sb.append(CRLF);
    return sb.toString();
  }//prepareMPFoot

  private static String generateBoundary() {
    StringBuilder sb = new StringBuilder();
    byte[] b = new byte[24];
    c_Random.nextBytes(b);
    sb.append(new BigInteger(b).abs().toString(36));
    return sb.toString();
  }//generateBoundary


  private static final String USER_AGENT = "User-Agent";
  private static final String USER_AGENT_STRING = "Arimdi AssetManager/1.0";
  private static final String CONTENT_TYPE = "Content-Type";
  private static final String HTTP_POST = "POST";
  private static final String HTTP_GET = "GET";
  private static final String CONTENT_LENGTH = "Content-Length";

  public static final String CONTENT_TYPE_XML = "text/xml; charset=utf-8";
  public static final String CONTENT_TYPE_JPG = "image/jpeg";
  public static final String CRLF = "\r\n";
}//HttpClient
