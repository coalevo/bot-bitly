/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.impl;

import static net.coalevo.bot.bitly.impl.BitlyConstants.*;

import java.io.InputStream;
import java.io.IOException;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

/**
 * This class implments a container for bit.ly provided stats.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BitlyStats
    implements StreamReadable {

  private int m_Error;
  private String m_ErrorMessage;
  private String m_Hash;
  private long m_Clicks;

  BitlyStats(String hash) {
    m_Hash = hash;
  }//constructor

  public int getError() {
    return m_Error;
  }//getError

  public String getErrorMessage() {
    return m_ErrorMessage;
  }//getErrorMessage

  public boolean notError() {
    return m_Error == 0;
  }//notError

  public String getHash() {
    return m_Hash;
  }//getHash

  public String getShort() {
    return BITLY_URL + m_Hash;
  }//getShort

  public long getClicks() {
    return m_Clicks;
  }//getClicks

  public void readFrom(InputStream in) throws IOException {
   JsonFactory f = new JsonFactory();
    JsonParser jp = f.createJsonParser(in);
    if (jp.nextToken() != JsonToken.START_OBJECT) {
      throw new IOException("Malformed stream.");
    }
    while (jp.nextToken() != JsonToken.END_OBJECT) {
      String fieldname = jp.getCurrentName();
      jp.nextToken(); // move to value
//      System.out.println("Field: " + fieldname);
      if (ERROR_CODE.equals(fieldname)) {
        m_Error = jp.getIntValue();
        if(m_Error != 0) {
          jp.nextToken(); //move to next field
          fieldname = jp.getCurrentName();
          if(ERROR_MESSAGE.equals(fieldname)) {
            jp.nextToken(); //move to value
            m_ErrorMessage = jp.getText();
          }
          //done parsing
          return;
        }
      } else if(ERROR_MESSAGE.equals(fieldname)) {
        m_ErrorMessage = jp.getText();
      } else if (RESULTS.equals(fieldname)) { // contains an object
        while (jp.nextToken() != JsonToken.END_OBJECT) {
          fieldname = jp.getCurrentName();
          jp.nextToken(); //move to value
          //System.out.println("SubField: " + fieldname);
          if(CLICKS.equals(fieldname)) {
            m_Clicks = jp.getLongValue();
          }
        }
      } else if(STATUS_CODE.equals(fieldname) && ! "OK".equals(jp.getText())) {
        throw new IOException("Failed operation.");
      }
    }
  }//readFrom

  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("BitlyStats");
    sb.append("{m_Hash='").append(m_Hash).append('\'');
    sb.append(", m_Clicks=").append(m_Clicks);
    sb.append('}');
    return sb.toString();
  }//toString

  /*
  public static void main(String[] args) {
   String str = "{\n" +
       "    \"errorCode\": 0, \n" +
       "    \"errorMessage\": \"\", \n" +
       "    \"results\": {\n" +
       "        \"clicks\": 3844, \n" +
       "        \"hash\": \"31IqMl\", \n" +
       "        \"referrers\": {\n" +
       "            \"\": {\n" +
       "                \"/index.html\": 1, \n" +
       "                \"None\": 5, \n" +
       "                \"direct\": 2690\n" +
       "            }, \n" +
       "            \"alpha.smx.local\": {\n" +
       "                \"/compositions\": 1\n" +
       "            }, \n" +
       "            \"api.bit.ly\": {\n" +
       "                \"/shorten\": 2\n" +
       "            }, \n" +
       "            \"benny-web\": {\n" +
       "                \"/jseitel/bitly/install.html\": 1\n" +
       "            }, \n" +
       "            \"bit.ly\": {\n" +
       "                \"/\": 4, \n" +
       "                \"/app/demos/info.html\": 187, \n" +
       "                \"/app/demos/stats.html\": 637, \n" +
       "                \"/app/demos/statsModule.html\": 7, \n" +
       "                \"/info/14tP6\": 1, \n" +
       "                \"/info/15DlK\": 1, \n" +
       "                \"/info/27I9Ll\": 1, \n" +
       "                \"/info/31IqMl\": 4, \n" +
       "                \"/info/J066u\": 1, \n" +
       "                \"/info/SiNn6\": 1, \n" +
       "                \"/info/hic4E\": 1\n" +
       "            }, \n" +
       "            \"code.google.com\": {\n" +
       "                \"/p/bitly-api/wiki/ApiDocumentation\": 2\n" +
       "            }, \n" +
       "            \"dev.chartbeat.com\": {\n" +
       "                \"/static/bitly.html\": 1\n" +
       "            }, \n" +
       "            \"dev.unhub.com\": {\n" +
       "                \"/pnG8/\": 1\n" +
       "            }, \n" +
       "            \"klout.net\": {\n" +
       "                \"/profiledetail.php\": 1\n" +
       "            }, \n" +
       "            \"localhost\": {\n" +
       "                \"/New Folder/test1.php\": 1, \n" +
       "                \"/SampleWeb/URLShortener.aspx\": 1, \n" +
       "                \"/index2.html\": 1, \n" +
       "                \"/tbazar/bitley.php\": 1\n" +
       "            }, \n" +
       "            \"localhost:3246\": {\n" +
       "                \"/urlcompress/default.aspx\": 1\n" +
       "            }, \n" +
       "            \"localhost:8888\": {\n" +
       "                \"/tweetlytics/ui.details.php\": 1\n" +
       "            }, \n" +
       "            \"mail.google.com\": {\n" +
       "                \"/mail/\": 1\n" +
       "            }, \n" +
       "            \"partners.bit.ly\": {\n" +
       "                \"/td\": 13\n" +
       "            }, \n" +
       "            \"powertwitter.me\": {\n" +
       "                \"/\": 3\n" +
       "            }, \n" +
       "            \"quietube.com\": {\n" +
       "                \"/getbitly.php\": 1\n" +
       "            }, \n" +
       "            \"search.twitter.com\": {\n" +
       "                \"/search\": 3\n" +
       "            }, \n" +
       "            \"sfbay.craigslist.org\": {\n" +
       "                \"/sfc/rnr/891043787.html\": 8\n" +
       "            }, \n" +
       "            \"spreadsheets.google.com\": {\n" +
       "                \"/ccc\": 40, \n" +
       "                \"/ccc2\": 1\n" +
       "            }, \n" +
       "            \"strat.corp.advertising.com\": {\n" +
       "                \"/brandmaker/bitly.php\": 4\n" +
       "            }, \n" +
       "            \"t4nk.org\": {\n" +
       "                \"\": 2\n" +
       "            }, \n" +
       "            \"taggytext.com\": {\n" +
       "                \"/ganja\": 1\n" +
       "            }, \n" +
       "            \"twitter.com\": {\n" +
       "                \"/\": 14, \n" +
       "                \"/SuperTestAcct\": 2, \n" +
       "                \"/TattoosOn\": 1, \n" +
       "                \"/WilliamWoods\": 1, \n" +
       "                \"/home\": 6, \n" +
       "                \"/ibiboisms\": 2, \n" +
       "                \"/kshanns\": 1, \n" +
       "                \"/matraji\": 1, \n" +
       "                \"/nathanfolkman\": 1, \n" +
       "                \"/pantaleonescu\": 1, \n" +
       "                \"/rubyisbeautiful\": 1, \n" +
       "                \"/spdean\": 1, \n" +
       "                \"/williamwoods\": 1, \n" +
       "                \"/yachay\": 2, \n" +
       "                \"/yachay/status/4447647278\": 1\n" +
       "            }, \n" +
       "            \"twitter.mattz.dev.buddymedia.com\": {\n" +
       "                \"/twitter/\": 1\n" +
       "            }, \n" +
       "            \"twitturls.com\": {\n" +
       "                \"/\": 21\n" +
       "            }, \n" +
       "            \"twitturly.com\": {\n" +
       "                \"\": 17, \n" +
       "                \"/urlinfo/url/2168a5e81280538cdbf6ad4c4ab019db/\": 1\n" +
       "            }, \n" +
       "            \"twubs.com\": {\n" +
       "                \"\": 1\n" +
       "            }, \n" +
       "            \"uploaddownloadperform.net\": {\n" +
       "                \"/Test/TestPage2\": 1\n" +
       "            }, \n" +
       "            \"url.ly\": {\n" +
       "                \"/\": 19, \n" +
       "                \"/info/27I9Ll\": 1, \n" +
       "                \"/info/31IqMl\": 3\n" +
       "            }, \n" +
       "            \"urly.local:3600\": {\n" +
       "                \"/\": 7\n" +
       "            }, \n" +
       "            \"v2.blogg.no\": {\n" +
       "                \"/test.cfm\": 1\n" +
       "            }, \n" +
       "            \"www.facebook.com\": {\n" +
       "                \"/home.php\": 1\n" +
       "            }, \n" +
       "            \"www.longurlplease.com\": {\n" +
       "                \"/\": 3\n" +
       "            }, \n" +
       "            \"www.microblogbuzz.com\": {\n" +
       "                \"\": 5\n" +
       "            }, \n" +
       "            \"www.mshare.net\": {\n" +
       "                \"/report/app\": 1\n" +
       "            }, \n" +
       "            \"www.tongs.org.uk\": {\n" +
       "                \"\": 1\n" +
       "            }\n" +
       "        }\n" +
       "    }, \n" +
       "    \"statusCode\": \"OK\"\n" +
       "}";
   
    try {
      BitlyStats bs = new BitlyStats("31IqMl");
      //System.out.println(bu.m_Long);
      bs.readFrom(new java.io.ByteArrayInputStream(str.getBytes("utf-8")));
      System.out.println(bs.toString());
    } catch (Exception ex) {
      ex.printStackTrace(System.out);
    }
  }//main

  */

}//class BitlyStats
