/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.impl;

import static net.coalevo.bot.bitly.impl.BitlyConstants.*;

import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;

import java.io.InputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * This class implments a container for bit.ly provided URL's.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BitlyURL implements StreamReadable {

  private int m_Error = 0;
  private String m_ErrorMessage = "";

  private String m_Short;
  private String m_ShortKeyword;
  private String m_Long;
  private String m_Hash;
  private String m_UserHash;

  private BitlyURL() {
    //prevents instantiation
  }//constructor

  public int getError() {
    return m_Error;
  }//getError

  public String getErrorMessage() {
    return m_ErrorMessage;
  }//getErrorMessage

  public boolean notError() {
    return m_Error == 0;
  }//notError

  public String getHash() {
    return m_Hash;
  }//getHash

  public String getLong() {
    return m_Long;
  }//getLong

  public String getShort() {
    if(m_Short == null || m_Short.length() == 0) {
      m_Short = BITLY_URL + m_Hash;
    }
    return m_Short;
  }//getShort

  public String getShortKeyword() {
    return m_ShortKeyword;
  }//getShortKeyword

  public String getUserHash() {
    return m_UserHash;
  }//getUserhash

  public void readFrom(InputStream in) throws IOException {
   JsonFactory f = new JsonFactory();
    JsonParser jp = f.createJsonParser(in);
    if (jp.nextToken() != JsonToken.START_OBJECT) {
      throw new IOException("Malformed stream.");
    }
    while (jp.nextToken() != JsonToken.END_OBJECT) {
      String fieldname = jp.getCurrentName();
      jp.nextToken(); // move to value
      if (ERROR_CODE.equals(fieldname)) {
        m_Error = jp.getIntValue();
        if(m_Error != 0) {
          jp.nextToken(); //move to next field
          fieldname = jp.getCurrentName();
          if(ERROR_MESSAGE.equals(fieldname)) {
            jp.nextToken(); //move to value
            m_ErrorMessage = jp.getText();
          }
          //done parsing
          return;
        }
      } else if(ERROR_MESSAGE.equals(fieldname)) {       
        m_ErrorMessage = jp.getText();
      } else if (RESULTS.equals(fieldname)) { // contains an object
        while (jp.nextToken() != JsonToken.END_OBJECT) {
          fieldname = jp.getCurrentName();
          jp.nextToken(); //move to value
          if (fieldname.equals(m_Long)) {  //contains object
            while (jp.nextToken() != JsonToken.END_OBJECT) {
              fieldname = jp.getCurrentName();
              jp.nextToken(); //move to value
              if (HASH.equals(fieldname)) {
                m_Hash = jp.getText();
              } else if(SHORT_URL.equals(fieldname)) {
                m_Short = jp.getText();
              } else if(USERHASH.equals(fieldname)) {
                m_UserHash = jp.getText(); 
              } else if(SHORT_KEYWORD_URL.equals(fieldname)) {
                m_ShortKeyword = jp.getText();
              }
              //ignore
            }
          } else if(fieldname.equals(m_Hash)) {
            while (jp.nextToken() != JsonToken.END_OBJECT) {
              fieldname = jp.getCurrentName();
              jp.nextToken(); //move to value
              if(LONG_URL.equals(fieldname)) {
                m_Long = jp.getText();
              }
            }
          } else {
            throw new IOException("Malformed stream.");
          }
        }
      } else if(STATUS_CODE.equals(fieldname) && ! "OK".equals(jp.getText())) {
        throw new IOException("Failed operation.");
      }
    }
  }//readFrom


  @Override
  public String toString() {
    final StringBuilder sb = new StringBuilder();
    sb.append("BitlyURL");
    sb.append("{m_Hash='").append(m_Hash).append('\'');
    sb.append(", m_Long='").append(m_Long).append('\'');
    sb.append(", m_Short='").append(m_Short).append('\'');
    sb.append(", m_ShortKeyword='").append(m_ShortKeyword).append('\'');
    sb.append(", m_UserHash='").append(m_UserHash).append('\'');
    sb.append('}');
    return sb.toString();
  }//toString

  static BitlyURL createShorten(String lurl) throws MalformedURLException {
    BitlyURL bu = new BitlyURL();
    bu.m_Long = new URL(lurl).toString();
    return bu;
  }//createShorten

  static BitlyURL createExpand(String hash) throws MalformedURLException {
    BitlyURL bu = new BitlyURL();
    bu.m_Hash = hash;
    return bu;
  }//createHashExpand

  /*
  public static void main(String[] args) {
   String str = "{\n" +
       "    \"errorCode\": 0,\n" +
       "    \"errorMessage\": \"\",\n" +
       "    \"results\": {\n" +
       "        \"http://cnn.com\": {\n" +
       "            \"hash\": \"31IqMl\",\n" +
       "            \"shortKeywordUrl\": \"\",\n" +
       "            \"shortUrl\": \"http://bit.ly/15DlK\",\n" +
       "            \"userHash\": \"15DlK\"\n" +
       "        }\n" +
       "    },\n" +
       "    \"statusCode\": \"OK\"\n" +
       "}";
    String str2 = "{\n" +
        "    \"errorCode\": 0, \n" +
        "    \"errorMessage\": \"\", \n" +
        "    \"results\": {\n" +
        "        \"15DlK\": {\n" +
        "            \"longUrl\": \"http://cnn.com/\"\n" +
        "        }\n" +
        "    }, \n" +
        "    \"statusCode\": \"OK\"\n" +
        "}";
    try {
      BitlyURL bu = createShorten("http://cnn.com");
      //System.out.println(bu.m_Long);
      bu.readFrom(new java.io.ByteArrayInputStream(str.getBytes("utf-8")));
      System.out.println(bu.toString());
      bu = createExpand("15DlK");
      //System.out.println(bu.m_Long);
      bu.readFrom(new java.io.ByteArrayInputStream(str2.getBytes("utf-8")));
      System.out.println(bu.toString());
    } catch (Exception ex) {
      ex.printStackTrace(System.out);
    }
  }//main

  */

}//class BitlyURL
