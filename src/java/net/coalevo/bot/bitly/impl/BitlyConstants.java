/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.impl;

/**
 * This class defines bit.ly API constants.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
class BitlyConstants {

  private BitlyConstants() {
    //prevents instantiation
  }//constructor

  static final String VERSION = "version";
  static final String API_KEY = "apiKey";
  static final String LOGIN = "login";
  static final String SHORT_URL="shortUrl";
  static final String LONG_URL = "longUrl";
  static final String SHORT_KEYWORD_URL = "shortKeywordUrl";
  static final String STATUS_CODE = "statusCode";

  static final String RESULTS = "results";
  static final String HASH = "hash";
  static final String USERHASH = "userHash";
  static final String GLOBALHASH = "globalHash";
  static final String CLICKS = "clicks";

  static final String HTML_META_DESC = "htmlMetaDescription";
  static final String HTML_META_KEYWORDS = "htmlMetaKeywords";
  static final String HTML_TITLE = "htmlTitle";
  static final String KEYWORD = "keyword";
  static final String KEYWORDS = "keywords";
  static final String SHORTENED_BY_USER = "shortenedByUser";

  static final String ERROR_CODE = "errorCode";
  static final String ERROR_MESSAGE = "errorMessage";

  static final String METHOD_SHORTEN = "shorten";
  static final String METHOD_EXPAND = "expand";
  static final String METHOD_STATS = "stats";
  static final String METHOD_INFO = "info";

  static final String BITLY_URL = "http://bit.ly/";
  static final String KEYS = "keys";
  static final String INFO_KEYS = "globalHash,htmlMetaDescription,htmlMetaKeywords,htmlTitle,keyword,keywords,longUrl,shortenedByUser,userHash";


}//class BitlyConstants
