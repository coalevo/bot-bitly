/***
 * Coalevo Project 
 * http://www.coalevo.net
 *
 * (c) Dieter Wimberger
 * http://dieter.wimpi.net
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 *
 * You may obtain a copy of the License at:
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ***/
package net.coalevo.bot.bitly.service;

/**
 * This is a tagging interface for the registration of the
 * unified bundle configuration.
 * <p/>
 *
 * @author Dieter Wimberger (wimpi)
 * @version @version@ (@date@)
 */
public interface BitlyBotConfiguration {

  public static final String BITLY_URL = "http://api.bit.ly";

  public static final String BITLY_API_VERSION = "2.0.1";
  
  public static final String BITLY_LOGIN_KEY = "bitly.login";
  
  public static final String BITLY_APIKEY_KEY = "bitly.apikey";

  public static final String BITLY_URLCACHESIZE_KEY = "bitly.url.cache.size";

  public static final String BITLY_INFOCACHESIZE_KEY = "bitly.info.cache.size";

}//interface BitlyBotConfiguration
